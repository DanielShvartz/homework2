#include "Ribosome.h"
#include "Protein.h"
#include "AminoAcid.h"
#include <iostream>

/*
The function creates a linked list which represents an protien, a linked list of amino acids
input; gets a rna transcript
output; returns a protien
*/
Protein * Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* newProtein = new Protein; // create a pointer to a new amino chain
	std::string rna = RNA_transcript;
	std::string colon; // colon to each 3 nocs
	AminoAcid amino_acid; // an amino acid that return 
	newProtein->init(); // initilaize a linked list;
	while (rna.length() >= 3) // while the length is more than 3
	{
		colon = rna.substr(0, 3); // we take the first 3 letters
		amino_acid = get_amino_acid(colon); // the amino acid that returned
		rna = rna.substr(3); // the rna transcript gets cut by 3 letters in the start
		if (amino_acid == UNKNOWN) // if it returns unknown
		{
			newProtein->clear(); // we clear the list
			return nullptr; // and return a null pointer
		}
		else
			newProtein->add(amino_acid); // if not we add the amino acid	
	}
	return newProtein;
}
