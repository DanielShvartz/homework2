#include "Nucleus.h"
#include <string>
#include <iostream>
#include <algorithm>	
/*
The initilalize function gets the data from outside and setting a DNA strand
input; the starting index of the dna strand, the end, and if the starnd is complementary or not
output; create a dna strand
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
The function returns the starting strand dna index
input; will get a this pointer to the gene
output; returns the starting strand dna index
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}
/*
The function returns the ending strand dna index
input; will get a this pointer to the gene
output; returns the ending strand dna index
*/
unsigned int Gene::get_end() const
{
	return this->_end;
}

/*
The function returns the if the dna strand is the complementary strand or not
input; will get a this pointer to the gene
output; return is the dna strand is complementary or not
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}
//**********************************************************************
/*
The function sets new dna strand starting index
input; will get a this pointer to the gene and a new start(which wont change)
output; sets new dna strand starting index
*/
void Gene::set_start(const unsigned int new_start)
{
	this->_start = new_start;
}
/*
The function sets new dna strand ending index
input; will get a this pointer to the gene and a new end index(which wont change)
output; sets new dna strand ending index
*/
void Gene::set_end(const unsigned int new_end)
{
	this->_end = new_end;
}
/*
The function sets new dna strand complementary or not
input; will get a this pointer to the gene and if the dna strand is complementary or not(which wont change)
output; sets new dna strand
*/
void Gene::set_complementary_dna_strand(const bool new_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = new_complementary_dna_strand;
}


//**********************************************************************
/*
The function initilalize a Nucleus
input; gets a dna sequence
output; create a dna complementary sequence and sets them to the nucleus
*/
void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence; // set the dna sequence
	std::string dna_complementary_sequence = ""; // create new dna sequence
	for (unsigned int i = 0; i < dna_sequence.length(); i++) // run on the dna sequence
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'C' && dna_sequence[i] != 'G' && dna_sequence[i] != 'T')
		{
			std::cerr << "Sequence letters must be ACGT!" << std::endl; // if its not agct we return an error
			_exit(1);
		}
		else
		{
			if (dna_sequence[i] == 'A') // if its a->t
				dna_complementary_sequence += 'T';
			else if (dna_sequence[i] == 'C') // if its c->g
				dna_complementary_sequence += 'G';
			else if (dna_sequence[i] == 'G') // if its g->c
				dna_complementary_sequence += 'C';
			else // if its not a not c and not g it must be t
				dna_complementary_sequence += 'A'; // t->a
		}
	}
	this->_complementary_DNA_strand = dna_complementary_sequence;
}

/*
The function returns a dna sequence so we can use it to copy a rna sequence, the function doesnt change the fields
input; the nucleus this pointer
output; returns a dna sequence
*/
std::string Nucleus::get_dna_sequence() const
{
	return this->_DNA_strand;
}
/*
The function returns a dna complementary sequence so we can use it to copy a rna sequence
the function doesnt change the fields
input; the nucleus this pointer
output; returns a dna complementary sequence
*/
std::string Nucleus::get_dna_complementary_sequence() const
{
	return this->_complementary_DNA_strand;
}

/*
The function creates a Rna sequence
input; a gene and a this pointer to a nucleus
output; creates and returns a rna sequence
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_sequence = "";
	
	if (gene.is_on_complementary_dna_strand()) // if it is the complementary strand
	{
		std::string DNA_complementary_sequence = this->get_dna_complementary_sequence(); // get the complementary sequence
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++) // copy from where needed
		{
			if (DNA_complementary_sequence[i] == 'T') // if its t
				RNA_sequence += 'U'; // make it u
			else
				RNA_sequence += DNA_complementary_sequence[i]; // of not just copy normally
		}
	}
	else // if its on the normal dan sequence
	{
		std::string DNA_sequence = this->get_dna_sequence(); // copy the dna sequence
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++) // copy what needed
		{
			if (DNA_sequence[i] == 'T') // if its t make it u
				RNA_sequence += 'U';
			else
				RNA_sequence += DNA_sequence[i]; // if not copy normally
		}
	}
	return RNA_sequence;
}

/*
The function returns a reverse dna strand
input; a this pointer to nucleus
output; returns a reverse dna strand
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string dna_strand = this->get_dna_sequence(); // get the dna strand from the nucleus field
	std::reverse(dna_strand.begin(), dna_strand.end()); // reverse not the original one from the start to the end
	return dna_strand; // return the copy
}

/*
The function returns how much times a codon was in a dna sequence
input; a this pointer to nucleus, and a codon string refrence
output; returns how much times codon occured in the dna sequence.
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string s = this->get_dna_sequence(); // get the string the sequence
	int count = 0; // counting
	size_t nPos = s.find(codon, 0); // first occurrence
	while (nPos != std::string::npos)
	{
		count++;
		nPos = s.find(codon, nPos + 1);
	}
	return count;
}
