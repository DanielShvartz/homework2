#pragma once
#include <string>

class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	// initialize
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	
	//getter
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	//setters
	void set_start(const unsigned int new_start);
	void set_end(const unsigned int new_end);
	void set_complementary_dna_strand(const bool new_complementary_dna_strand);
};

class Nucleus
{
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

	//getters
	std::string get_dna_sequence() const;
	std::string get_dna_complementary_sequence() const;

};