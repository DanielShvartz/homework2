#include "Cell.h"
#include <iostream>
/*
The function initializes a cell
input; the dna squence and a glucose recptor gene
output; initializes a cell
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
	this->_nucleus.init(dna_sequence);
}
/*
The function calls all the functions needed and checks if it can create energy
input; the cell
output; return if can create enegry
*/
bool Cell::get_ATP()
{
	std::string rnaTranscript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* my_protein = this->_ribosome.create_protein(rnaTranscript);
	if (my_protein == nullptr)
	{
		std::cerr << "Cannot create protien" << std::endl;
		_exit(1);
		return false;
	}
	else
	{
		this->_mitochondrion.insert_glucose_receptor(*my_protein);
		this->_mitochondrion.set_glucose(50);
		bool can_make_atp = this->_mitochondrion.produceATP(50);
		if (can_make_atp == false)
			return false;
		else
		{
			_atp_units = 100;
			return true;
		}
	}
	
}
