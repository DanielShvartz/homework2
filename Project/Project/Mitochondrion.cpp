#include "Mitochondrion.h"

/*
The function initialize a Mitochondrion
input; a this pointer to a Mitochondrion
output; sets the glocuse level to 0 and sets the receptor to false
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*
The function checks if the protien is this:
ALANINE-LEUCINE-GLYCINE-HISTIDINE-LEUCINE-PHENYLALANINE-AMINO_CHAIN_END
input; the protein
output; sets the receptor
*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* curr = protein.get_first(); // we get the first
	AminoAcid wantedAminoAcid[] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END };
	int i = 0, counterAcids = 0;
	// and create an array which stores all the needed amino acids
	while (curr) // we run till the end
	{
		if (curr->get_data() == wantedAminoAcid[i]) // if it has the wanted amino acid
			counterAcids++; // we add the counter
		else
			break; // i muse be == to counter acids to when they are not the same we can stop the loop
		i++; // move to the next acid
		curr = curr->get_next(); // move to the next amino
	}
	if (i == counterAcids) // if they are the same
		this->_has_glocuse_receptor = true; // change to true
}

/*
The function sets a new glucose level
input; a this pointer to a Mitochondrion and a glocuse level
output; sets the new glucose level
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
The function checks if it can produce atp
input; wanted glocuse level and a pointer to the Mitochondrion
output; returns if the Mitochondrion can produce an atp
*/
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	return (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50);
	// returning if it has a receptor and the glocuse level is above 50
}